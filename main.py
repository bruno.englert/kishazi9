import numpy as np
from matplotlib import pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.image as mimage

from mlp import MLP
from mnist import MNIST
import os


if __name__ == "__main__":
    mndata = MNIST(os.path.expanduser('mnist'))
    # Load training MNIST images and labels
    images, labels = mndata.load_training()
    # Convert to numpy float32 arrays
    images = np.array(images, dtype=np.float32)
    labels = np.array(labels, dtype=np.float32)
    # Load testing MNIST images and labels
    val_images, val_labels = mndata.load_testing()
    # Convert to numpy float32 arrays
    val_images = np.array(val_images, dtype=np.float32)
    val_labels = np.array(val_labels, dtype=np.float32)
    # Scaling each pixels between 0.0 and 1.0
    images = images / 255.0
    val_images = val_images / 255.0

    # Wrapping everyting up into a single variable
    rows, _ = images.shape
    samples = np.zeros(rows, dtype=[('input', float, 784), ('output', float, 10)])
    for i in range(rows):
        samples[i] = images[i], np.eye(10)[int(labels[i])]

    # Wrapping everyting up into a single variable
    rows, _ = val_images.shape
    validation_samples = np.zeros(rows, dtype=[('input', float, 784), ('output', int, 1)])
    for i in range(rows):
        validation_samples[i] = val_images[i], val_labels[i]

    print("Image dim: {} Target dim: {}".format(images.shape, labels.shape))

    gs = gridspec.GridSpec(2, 5, top=0.8, bottom=0.1, right=1., left=0.0, hspace=0.5,
                           wspace=0.)
    for idx, g in enumerate(gs):
        ax = plt.subplot(g)
        ax.imshow(images[idx].reshape(28, 28), cmap='gray')
        ax.set_xticks([])
        ax.set_yticks([])
    plt.show()

    # Creating a model
    mlp = MLP(batch_size=64, shape=[784, 784, 512, 10])

    # Traning
    mlp.train(samples=samples, validation_samples=validation_samples, epochs=1, lrate=0.01, momentum=0.1)

    # Displaying some predictions
    predictions = mlp.propagate_forward(validation_samples['input'])

    for row in range(5):
        fig, ax = plt.subplots(figsize=(2, 2))
        img = (validation_samples['input'][row])
        ax.imshow(img.reshape(28, 28), cmap='gray')
        ax.set_xlabel('prediction: {}'.format(str(np.argmax([predictions[row]]))))
        ax.set_xticks([])
        ax.set_yticks([])
        plt.show()
