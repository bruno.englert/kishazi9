import numpy as np
import activation
from progressbar import pbprint


class MLP(object):
    # Model init
    def __init__(self, batch_size, shape):
        # adding random seed
        np.random.seed(123)
        # Epsilon is needed to random init the weights and biases.
        # It scales the inital weight and bias values between -epsilon and +epsilon
        self.epsilon = 0.2
        # batch size for the mini batch gradient
        self.batch_size = batch_size
        # Shape of the model given by a list
        self.shape = shape
        # Number of layers
        self.no_layers = len(shape)
        # Creating the list for the layers
        self.layers = []
        self.delta_layers = []
        for i in range(self.no_layers):
            # Creating the input layer, hidden layer(s) and the output layer
            self.layers.append(np.ones((batch_size, self.shape[i]), dtype=np.float))
            # Creating the same layer structure for the backpropagation
            self.delta_layers.append(np.ones((batch_size, self.shape[i]), dtype=np.float))
        # Creating the weight, delta weight and bias matrices
        self.weights = []
        self.delta_weights = []
        self.bias = []
        for i in range(self.no_layers - 1):
            self.weights.append(self.random_matrix(self.shape[i], self.shape[i + 1]))
            self.delta_weights.append(self.random_matrix(self.shape[i], self.shape[i + 1]))
            self.bias.append(self.random_matrix(1, self.shape[i + 1]))
        for i in range(len(self.weights)):
            print("layer{}: weight shape: {} bias size: {}".format(i, self.weights[i].shape, self.bias[i].shape))

            # Inicializing randomly a matix. The values are between -epsilon and +epsilon

    def random_matrix(self, rows, cols):
        # Random numbers in the range of [0,1)
        Z = np.random.random((rows, cols))
        # Scaling
        return (2 * Z * self.epsilon) - self.epsilon

    # Input data propagates through the model
    def propagate_forward(self, data):
        # Setting the input layer
        self.layers[0] = data

        for i in range(self.no_layers - 2):
            self.layers[i + 1] = np.dot(self.layers[i], self.weights[i])
            # Adding the bias
            self.layers[i + 1] += self.bias[i]
            # Activation function. In this case a ReLU
            self.layers[i + 1] = activation.relu(self.layers[i + 1])

        # Output layer
        i = self.no_layers - 2
        self.layers[i + 1] = np.dot(self.layers[i], self.weights[i])
        # Adding the bias
        self.layers[i + 1] += self.bias[i]
        # Activation function for the output is sigmoid
        self.layers[i + 1] = activation.sigmoid(self.layers[i + 1])
        # Returning the last layer
        return self.layers[-1]

    # Target propagates backward the model
    def propagate_backward(self, target, momentum):
        # Calculating the error on the last layer
        error = (self.layers[-1] - target)  # y-target
        # Setting the delta on the last delta layer
        self.delta_layers[-1] = error * 2.0 * self.lrate
        # Calculating the gradient on the output layer
        i = self.no_layers - 2
        self.delta_layers[i] = np.multiply(np.dot(self.delta_layers[i + 1], self.weights[i].T),
                                           activation.sigmoid(self.layers[i]))
        delta_weights = np.dot(self.layers[i].T, self.delta_layers[i + 1]) / float(self.batch_size)
        delta_weights += momentum * self.delta_weights[i]
        self.delta_weights[i] = delta_weights

        # Calculating the gradient on the output layer
        for i in reversed(range(self.no_layers - 2)):
            self.delta_layers[i] = np.multiply(np.dot(self.delta_layers[i + 1], self.weights[i].T),
                                               activation.de_relu(self.layers[i]))
            delta_weights = np.dot(self.layers[i].T, self.delta_layers[i + 1]) / float(self.batch_size)
            # Using the momentum method
            delta_weights += momentum * self.delta_weights[i]
            self.delta_weights[i] = delta_weights
        # Súlyok módosítása
        for j in range(self.no_layers - 1):
            # Updating the weights and the biases
            self.weights[j] -= self.delta_weights[j]
            self.bias[j] -= np.sum(self.delta_layers[j + 1], axis=0) / float(self.batch_size)

        # Returning the error
        return (error ** 2).sum()

    def train(self, samples, validation_samples=None, epochs=10, lrate=1, momentum=0.9):
        # init lrate and momentum
        self.lrate = lrate
        self.momentum = momentum
        # epoch:
        for i in range(epochs):
            # Number of samples
            no_samples = len(samples['input'])
            # Init error before the epoch
            error = 0
            # Randomizing the order of the samples
            np.random.shuffle(samples)
            # Iterating through the samples
            for sample_idx in range(0, no_samples, self.batch_size):
                # Propagating forward a batch of samples
                self.propagate_forward(samples['input'][sample_idx:sample_idx + self.batch_size])
                # Propagating backward a batch of targets
                error += self.propagate_backward(samples['output'][sample_idx:sample_idx + self.batch_size],
                                                 momentum=momentum)
                #
                pbprint(sample_idx + 1, no_samples, prefix='Progress:', suffix='Complete', length=50)

            #
            print("\n{}/{} error: {}, learning rate: {}".format(i + 1, epochs, error, self.lrate))
            # Exponentially decaying the learning rate
            self.lrate = self.lrate * 0.9
            # Exponentially increasing the momentum rate
            self.momentum = self.momentum * 1.01
            # Validating after each epoch of traning
            if validation_samples is not None:
                # Getting the rows and cols of the validation samples
                rows, cols = validation_samples['input'].shape
                # Predicting with the model
                predictions = self.propagate_forward(validation_samples['input'])
                #
                correct = 0
                # Iterating through each validation sample
                for row in range(rows):
                    prediction_onehot = predictions[row]
                    # Converting a one-hot vector to a number
                    prediction_number = np.argmax(prediction_onehot)
                    # Checking if the prediction was correct
                    if (prediction_number - validation_samples['output'][row]) == 0:
                        correct += 1
                print("accuracy: {}".format(float(correct) / rows))

